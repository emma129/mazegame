import javax.swing.*;
import javax.swing.text.View;
import java.awt.*;
import java.util.*;


/**
 * Created by yzhb363 on 21/04/2017.
 */
public class MazeMap extends JFrame {

    private int[][] maze =
                   {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, // start position at [1,1]
                    {1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},
                    {1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1},
                    {1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1},
                    {1, 0, 1, 0, 0, 0, 9, 0, 1, 1, 1, 0, 1},
                    {1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 1},
                    {1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1},
                    {1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1},
                    {1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1},
                    {1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1},
                    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},


            };//two dimensional array to represent the maze

    /**
     * maze [row][col]
     * <p>
     * values :  0 = not- visited node
     * 1 = wall
     * 2 = visited node
     * 9 = target node
     */


    public MazeMap() {
        setTitle("MazeMap");
        setSize(900,600);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    @Override
    public void paint(Graphics graphics){
        super.paint(graphics);

        graphics.translate(50,50);


        for (int row = 0; row <maze.length ; row++){
            for (int col = 0; col<maze[0].length ; col++){
                Color color;
                switch (maze[row][col]){
                    case 1 : color = Color.BLACK;
                    break;
                    case 9 : color = Color.RED;
                    break;
                    default: color = Color.WHITE;

                }
                graphics.setColor(color);
                graphics.fillRect(40*col, 40*row,40,40);
                graphics.setColor(color);
                graphics.drawRect(40*col, 40*row,40,40);

            }
        }


    }



    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MazeMap view = new MazeMap() ;
                view.setVisible(true);
//
            }
        });
    }

}





